# Code 'n Craft Web App
Features the tools & timers used for Code 'n Craft events. Made with D3.


## Live Site
https://amitthekumar.gitlab.io/code-n-craft/


## Local Development

### Project Setup

``` shell
$ npm install
```

### Run

``` shell
$ npm start
```
This starts a browserSync server. View the site at http://localhost:3000.


## Author
Amit Kumar
http://www.amitkumar.com

## Image Inspirations
- https://gitlab.com/amitthekumar/code-n-craft/tree/master/public/img/kawandeep-virdee
- https://www.google.com/search?q=sol+lewitt

## References
- https://www.browsersync.io/docs/recipes
- https://bl.ocks.org/mbostock/1346410
- https://bl.ocks.org/mbostock/4341417
- http://webpack.github.io/docs/tutorials/getting-started/
- https://github.com/harytkon/d3-es6-webpack-boilerplate/blob/master/package.json
- http://webpack.github.io/docs/tutorials/getting-started/
